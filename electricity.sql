-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.6-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para electricity
CREATE DATABASE IF NOT EXISTS `electricity` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `electricity`;

-- Volcando estructura para tabla electricity.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `idcliente` bigint(20) NOT NULL,
  `nombrecliente` varchar(100) NOT NULL DEFAULT '',
  `direccion` varchar(100) NOT NULL DEFAULT '',
  `telefono` varchar(10) NOT NULL DEFAULT '',
  `tipo` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla electricity.cliente: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`idcliente`, `nombrecliente`, `direccion`, `telefono`, `tipo`) VALUES
	(1233, 'SANTIAGO LOPEZ', 'MONTERIA', '3235956003', 'PERSONA NATURAL');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Volcando estructura para tabla electricity.consumo
CREATE TABLE IF NOT EXISTS `consumo` (
  `idconsumo` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` bigint(20) NOT NULL,
  `iddispositivo` bigint(20) NOT NULL,
  `consumodiario` float NOT NULL,
  `consumomensual` float NOT NULL,
  `tiempouso` int(11) NOT NULL,
  `costoaproximado` float NOT NULL,
  `tipouso` varchar(100) NOT NULL,
  PRIMARY KEY (`idconsumo`),
  KEY `FK_consumo_cliente` (`idcliente`),
  KEY `FK_consumo_dispositivo` (`iddispositivo`),
  CONSTRAINT `FK_consumo_cliente` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`),
  CONSTRAINT `FK_consumo_dispositivo` FOREIGN KEY (`iddispositivo`) REFERENCES `dispositivo` (`iddispositivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla electricity.consumo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `consumo` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumo` ENABLE KEYS */;

-- Volcando estructura para tabla electricity.dispositivo
CREATE TABLE IF NOT EXISTS `dispositivo` (
  `iddispositivo` bigint(20) NOT NULL,
  `nombredispositivo` varchar(100) NOT NULL,
  `watts` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`iddispositivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla electricity.dispositivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispositivo` ENABLE KEYS */;

-- Volcando estructura para tabla electricity.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla electricity.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idusuario`, `usuario`, `clave`, `fechacreacion`, `tipo`) VALUES
	(1, 'admin', 'admin', '2019-11-23 10:04:15', 'ADMINISTRADOR'),
	(2, 'thiago', 'thiago', '2019-11-23 10:35:39', 'CLIENTE');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
