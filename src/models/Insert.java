package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Insert {
    
    public void addDevice(double id, String nombre, int cant, float wat) throws SQLException
    {
        try
        {
            Connect connect=new Connect();
            connect.Obtener();
            Connection conn= connect.getConexion();

            String query = "INSERT INTO dispositivo (iddispositivo, nombredispositivo, watts, cantidad) VALUES ( ?, ?, ?, ? )";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setDouble (1, (id));
            preparedStmt.setString (2, nombre);
            preparedStmt.setFloat  (3, wat);
            preparedStmt.setInt    (4, cant);
            
            preparedStmt.execute();
            conn.close();
            
            JOptionPane.showMessageDialog(null, "Dispositivo Agregado", "Electricity consumption", JOptionPane.INFORMATION_MESSAGE);
            
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar dispositivo", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    public void addConsumo(String cliente, String dispositivo)
    {
        Select select = new Select();
        
        try
        {
            Connect connect=new Connect();
            connect.Obtener();
            Connection conn= connect.getConexion();
            
            String Query = "SELECT * FROM dispositivo WHERE iddispositivo = '" + select.BuscarIdDevice(dispositivo) + "'";
            Statement st = conn.createStatement();

            ResultSet resultSet = st.executeQuery(Query);
            float wa = 0;
            int cant=0;
            while(resultSet.next())
            {
                wa = resultSet.getFloat("watts") ;
                cant = resultSet.getInt("cantidad");
            } 
        
        
            String query = "INSERT INTO consumo VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString (1, null);
            preparedStmt.setLong   (2, select.BuscarID(cliente) );
            preparedStmt.setLong   (3, select.BuscarIdDevice(dispositivo) );
            preparedStmt.setFloat  (4, (wa*cant) * 24);
            preparedStmt.setFloat  (5, (wa*cant) * 720);
            preparedStmt.setInt    (6, 720);
            preparedStmt.setFloat  (7, (wa*cant)*40 );
            preparedStmt.setString (8, "");
            
            
            
            
            
            preparedStmt.execute();
            conn.close();
            
            JOptionPane.showMessageDialog(null, "Consumo Agregado", "Electricity consumption", JOptionPane.INFORMATION_MESSAGE);
            
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar consumo", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
        
    }
    
    public void addClient(double id, String nombre, String dir, String tel, String tip) throws SQLException
    {
        try
        {
            Connect connect=new Connect();
            connect.Obtener();
            Connection conn= connect.getConexion();

            String query = "INSERT INTO cliente VALUES ( ?, ?, ?, ?, ? )";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setDouble (1, (id));
            preparedStmt.setString (2, nombre);
            preparedStmt.setString (3, dir);
            preparedStmt.setString (4, tel);
            preparedStmt.setString (5, tip);
            
            preparedStmt.execute();
            conn.close();
            
            JOptionPane.showMessageDialog(null, "Cliente Agregado", "Electricity consumption", JOptionPane.INFORMATION_MESSAGE);
            
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar dispositivo", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }
}
