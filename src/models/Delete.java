
package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Delete {
    public void Eliminarclient(DefaultTableModel modelo, JTable table, long ide) throws SQLException
    {
        try
        {
            Connect connect=new Connect();
            connect.Obtener();
            Connection conn = connect.getConexion();

            String Query = "DELETE FROM cliente where idcliente = ? ";
            PreparedStatement preparedStatement = conn.prepareStatement(Query);
            preparedStatement.setLong(1, ide);
            
            preparedStatement.execute();
            
            JOptionPane.showMessageDialog(null, "TABLA ACTUALIZADA" );
            Select select = new Select();
            select.VaciarTabla(table);
            select.ActualizarTablaClient(modelo, table);
            conn.close();
            
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"NO SE HA PODIDO ELIMINAR" , "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    public void EliminarDevice(DefaultTableModel modelo, JTable table, long ide) throws SQLException
    {
        try
        {
            Connect connect=new Connect();
            connect.Obtener();
            Connection conn = connect.getConexion();

            String Query = "DELETE FROM dispositivo where iddispositivo = ? ";
            PreparedStatement preparedStatement = conn.prepareStatement(Query);
            preparedStatement.setLong(1, ide);
            
            preparedStatement.execute();
            
            JOptionPane.showMessageDialog(null, "TABLA ACTUALIZADA" );
            Select select = new Select();
            select.VaciarTabla(table);
            select.ActualizarTablaClient(modelo, table);
            conn.close();
            
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"NO SE HA PODIDO ELIMINAR" , "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
    }
}
