/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;


/**
 *
 * @author sell3
 */
public class Connect {
    
    private Connection conexion;
    
    
        public Connect Obtener()
        {
          String _DATABASE="jdbc:mysql://localhost:3306/electricity";
          String _USER="root";
          String _PASS="";
          try{
                Class.forName("com.mysql.jdbc.Driver");
                setConexion(DriverManager.getConnection(_DATABASE, _USER, _PASS));

                if(getConexion() == null)
                {
                    JOptionPane.showMessageDialog(null, "NO SE HA ESTABLECIDO CONEXION A LA BASE DE DATOS :(");
                }

            } catch (Exception e) {
                  e.printStackTrace();
            }   
          return this;
        }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
    public ResultSet consultar(String sql) {
        ResultSet resultado;
        try {
            Statement sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);
        } catch (SQLException e) {
            return null;
        }        
        return resultado;
    }
    
}    
