
package models;

import com.mysql.jdbc.PreparedStatement;
import controllers.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Select {
    
    public int login(String usuario, String password, String tipo)throws SQLException {
     try {
         Connect conexion = new Connect().Obtener();
         if(tipo.equals("CLIENTE") ){
                ResultSet resultado = 
                        conexion.consultar("SELECT * FROM usuarios WHERE usuario = '" + usuario + "' AND clave = '" + password + "' AND tipo='" + tipo + "' " );
                resultado.last();
                if (resultado.getRow() > 0){
                    Usuario usuarioactual = Usuario.getInstance();
                     usuarioactual.setUsuario(resultado.getString("usuario"));
                     usuarioactual.setClave(resultado.getString("clave"));
                     usuarioactual.setFecha(resultado.getString("fechacreacion"));
                     usuarioactual.setIdUsuario(resultado.getInt("idusuario") );
                     usuarioactual.setTipo(resultado.getString("tipo"));
                    return 1;
               }
         }else if(tipo.equals("ADMINISTRADOR"))
         {
             ResultSet resultado = 
                     conexion.consultar("SELECT * FROM usuarios WHERE usuario = '" + usuario + "' AND clave = '" + password + "' AND tipo='" + tipo + "' " );
                resultado.last();
                if (resultado.getRow() > 0){
                    Usuario usuarioactual = Usuario.getInstance();
                     usuarioactual.setUsuario(resultado.getString("usuario"));
                     usuarioactual.setClave(resultado.getString("clave"));
                     usuarioactual.setFecha(resultado.getString("fechacreacion"));
                     usuarioactual.setIdUsuario(resultado.getInt("idusuario") );
                     usuarioactual.setTipo(resultado.getString("tipo"));
                    return 2;
               }
         }
         
    }catch (SQLException e) 
    {
        return -1;
    }
      return -1;
    }

    public void ActualizarTabla(DefaultTableModel modelo) throws SQLException
    {
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT * FROM dispositivo";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            long id = resultSet.getLong("iddispositivo");
            
            
            String nombre = resultSet.getString("nombredispositivo");
            float watt = resultSet.getFloat("watts");
            int cant = resultSet.getInt("cantidad");
            
            Object [] row = new Object[] {id, nombre, watt, cant};            
            modelo.addRow(row);
        }      
        
    }
    
    public void ActualizarTablaClient(DefaultTableModel modelo, JTable table) throws SQLException
    {
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT * FROM cliente";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            long id = resultSet.getLong("idcliente");
            
            
            String nombre = resultSet.getString("nombrecliente");
            String dir = resultSet.getString("direccion");
            String tel = resultSet.getString("telefono");
            String tip = resultSet.getString("tipo");
            
            Object [] row = new Object[] {id, nombre, dir, tel, tip };            
            modelo.addRow(row);
        }      
        
    }
    
    public void ActualizarTablaConsumo(DefaultTableModel modelo, JTable table) throws SQLException
    {
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT * FROM cliente INNER JOIN dispositivo INNER JOIN consumo ON cliente.idcliente=consumo.idcliente AND dispositivo.iddispositivo=consumo.iddispositivo  ";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            long id = resultSet.getLong("idcliente");
            
            
            String nombre = resultSet.getString("nombrecliente");
            String dispositivo = resultSet.getString("nombredispositivo");
            float consumodiario = resultSet.getFloat("consumodiario");
            float consumomensual = resultSet.getFloat("consumomensual");
            int tiempouso = resultSet.getInt("tiempouso");
            float costoaproximado = resultSet.getFloat("costoaproximado");
            String tipo = resultSet.getString("tipo");
            Object [] row = new Object[] {dispositivo, nombre, consumodiario, consumomensual, tiempouso, costoaproximado, tipo };            
            modelo.addRow(row);
        }      
        
    }
    
    public void BuscarClient(DefaultTableModel modelo, JTable table, long ide) throws SQLException
    {
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT * FROM cliente WHERE idcliente = '" + ide + "'";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            long id = resultSet.getLong("idcliente");
            
            
            String nombre = resultSet.getString("nombrecliente");
            String dir = resultSet.getString("direccion");
            String tel = resultSet.getString("telefono");
            String tip = resultSet.getString("tipo");
            
            Object [] row = new Object[] {id, nombre, dir, tel, tip };            
            modelo.addRow(row);
        }      
        
    }
    
    
    public void BuscarDevice(DefaultTableModel modelo, JTable table, long ide) throws SQLException
    {
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT * FROM dispositivo WHERE iddispositivo = '" + ide + "'";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            long id = resultSet.getLong("iddispositivo");
            
            
            String nombre = resultSet.getString("nombredispositivo");
            float wa = resultSet.getFloat("watts") ;
            int cant = resultSet.getInt("cantidad");
            
            Object [] row = new Object[] {id, nombre, wa, cant };            
            modelo.addRow(row);
        }      
        
    }
    
    public void VaciarTabla(JTable tabla)
    {
        if(tabla.getRowCount() == 0)
        {
            JOptionPane.showMessageDialog(null, "LA TABLA ESTA VACIA");
        }else
        {
            try {
                DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
                int filas = modelo.getRowCount();
                for(int i =0; filas>i; i++)
                {
                    modelo.removeRow(0);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,"HA OCURRIDO UN ERROR", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    
    
    public void CargarClientes(JComboBox clientes)
    {
        Connect cnConnect=new Connect();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        
        
        String SSQL = "SELECT nombrecliente FROM cliente ORDER BY nombrecliente ASC";

        try {
            cnConnect.Obtener();
            connection = cnConnect.getConexion();
            
            preparedStatement = (PreparedStatement) connection.prepareStatement(SSQL);            
            //Ejecutamos la consulta
            resultSet = preparedStatement.executeQuery();
            
            clientes.addItem("Seleccione un cliente");
   
            while(resultSet.next())
            {
                clientes.addItem(resultSet.getString("nombrecliente"));                
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{

            if(connection !=null){

                try {

                    connection.close();
                    resultSet.close();

                    connection = null;
                    resultSet = null;

                } catch (SQLException ex) {

                    JOptionPane.showMessageDialog(null, ex);

                }

            }       
        
        }
        
   }
    
    public void CargarDispositivos(JComboBox clientes)
    {
        Connect cnConnect=new Connect();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        
        
        String SSQL = "SELECT nombredispositivo FROM dispositivo ORDER BY nombredispositivo ASC";

        try {
            cnConnect.Obtener();
            connection = cnConnect.getConexion();
            
            preparedStatement = (PreparedStatement) connection.prepareStatement(SSQL);            
            //Ejecutamos la consulta
            resultSet = preparedStatement.executeQuery();
            
            clientes.addItem("Seleccione un dispositivo");
   
            while(resultSet.next())
            {
                clientes.addItem(resultSet.getString("nombredispositivo"));                
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }finally{

            if(connection !=null){

                try {

                    connection.close();
                    resultSet.close();

                    connection = null;
                    resultSet = null;

                } catch (SQLException ex) {

                    JOptionPane.showMessageDialog(null, ex);

                }

            }       
        
        }
        
   }
    
    
    public long BuscarID(String nombre) throws SQLException
    {
        int id = -1;
        
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT idcliente FROM cliente WHERE nombrecliente = '" + nombre + "'";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            id = resultSet.getInt("idcliente");
        } 
        return id;
    }
    
    public long BuscarIdDevice(String dispositivo) throws SQLException
    {
        int id = -1;
        
        Connect connect=new Connect();
        connect.Obtener();
        Connection conn = connect.getConexion();
        
        String Query = "SELECT iddispositivo FROM dispositivo WHERE nombredispositivo = '" + dispositivo + "'";
        Statement st = conn.createStatement();
        
        ResultSet resultSet = st.executeQuery(Query);
        
        while(resultSet.next())
        {
            id = resultSet.getInt("iddisoisitivo");
        } 
        return id;
    }
    
}
