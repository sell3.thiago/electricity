
package controllers;

public class Device extends Clientes {
    private long iddispositivo;
    private String nombre;
    private int cantidad;
    private float watts;
    private float consumodiario;
    private float consumomensual;
    private int tiempouso;
    private String tipouso;
    
    
    
    public Device()
    {
        iddispositivo = 0;
        nombre = "";
        cantidad= 0;
        watts = 0;
        consumodiario = 0;
        consumomensual = 0;
        tiempouso = 0;
        tipouso = "";
    }

    
    public long getIddispositivo() {
        return iddispositivo;
    }

    public void setIddispositivo(long iddispositivo) {
        this.iddispositivo = iddispositivo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getWatts() {
        return watts;
    }

    public void setWatts(float watts) {
        this.watts = watts;
    }

    public float getConsumodiario() {
        return consumodiario;
    }

    public void setConsumodiario(float consumodiario) {
        this.consumodiario = consumodiario;
    }

    public float getConsumomensual() {
        return consumomensual;
    }

    public void setConsumomensual(float consumomensual) {
        this.consumomensual = consumomensual;
    }

    public int getTiempouso() {
        return tiempouso;
    }

    public void setTiempouso(int tiempouso) {
        this.tiempouso = tiempouso;
    }

    public String getTipouso() {
        return tipouso;
    }

    public void setTipouso(String tipouso) {
        this.tipouso = tipouso;
    }
    
    
    
}
