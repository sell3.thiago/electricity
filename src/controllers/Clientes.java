
package controllers;


public class Clientes {
    private long idcliente;
    private String nombre;
    private String direccion;
    private String telefono;
    private String tipo;
    
    public static Clientes instance;
    
    public Clientes()
    {
        idcliente=0;
        nombre="";
        direccion="";
        telefono="";
        tipo = "";
    }

    public long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(long idcliente) {
        this.idcliente = idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public static Clientes getInstance() {
        if (instance == null)
            instance = new Clientes();
        return instance;
    }
}
