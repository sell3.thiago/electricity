package controllers;

import java.util.ArrayList;


public class Factura {
    ArrayList<Device> dispositivos =new ArrayList<Device>();
    
    public int tam()
    {
        return dispositivos.size();
    }
    
    public void add(Device val)
    {
        dispositivos.add(val);
    }
    
    public void Vaciar()
    {
        dispositivos.clear();
    }
    
    public void Eliminar(int dato)
    {
        for(int i=0; i<tam(); i++)
        {
            if(dispositivos.get(i).getIddispositivo() == dato)
            {
                dispositivos.remove(i);
            }
        }
    }
}
